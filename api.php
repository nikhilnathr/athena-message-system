<?php
$users = array();
$users = explode(PHP_EOL, shell_exec("users"));
$users = explode(" ", $users[0]); 
$users  =array_unique($users);
$unluckyUser = $users[array_rand($users,1)];

header('Content-Type: application/json');

if(!isset($_SERVER['REMOTE_ADDR'])){
	echo '{"success":"false","error":"cannotGetIP"}';
	exit;
}

$line = date('Y-m-d H:i:s') . " - $_SERVER[REMOTE_ADDR] - {\"GET\":". json_encode($_GET) .", \"POST\":". json_encode($_POST) ."}";

file_put_contents('./visitors.log', $line . PHP_EOL, FILE_APPEND);


if($_SERVER['REQUEST_METHOD']==="GET"){
	if(isset($_GET['activeUsers'])){
		$us=array();
		foreach($users as $user){
			array_push($us, $user);	
		}
		echo '{"success":"true","activeUsers":'. json_encode($us). '}';
		exit;
	}else if(isset($_GET['allUsers'])){
	    $allUsers = array();
	    $allUsers = explode(PHP_EOL, shell_exec("cat /etc/passwd | cut -d ':' -f1"));
	    array_pop($allUsers);
	    $groups = array();
	    $groups['sudo'] = explode(PHP_EOL, shell_exec("cat /etc/group | grep 'sudo' | cut -d ':' -f4 | tr , '\n'"));

        foreach($allUsers as $user){
            $gp = explode("_", $user);
            if(is_array($gp) && isset($gp[1]) && strpos($gp[1],"b")===0){
                if(array_key_exists("b20".substr($gp[1], 1, 2), $groups)){
                    array_push($groups["b20".substr($gp[1], 1, 2)], $user);
                }else{
                    $groups["b20".substr($gp[1], 1, 2)] = array();
                    array_push($groups["b20".substr($gp[1], 1, 2)], $user);
                }
            }
		}
		$restUsers=array();
		foreach($allUsers as $user){
			$pre=false;
			foreach($groups as $group){
				if(!in_array($user, $group)){
					$pre=true;
				}
			}
			if(!$pre){
				array_push($groups['rest'], $user);
			}
		}	
	    $displayJSON = '{"success":"true","groups":'. json_encode($groups) .'}';
	    echo $displayJSON;
	    exit;
	}else if(isset($_GET["search"]) && !empty($_GET["search"])){
		$keyword = trim(htmlspecialchars(stripslashes($_GET["search"])));
		$resultPeople = array();

	    $allUsers = array();
	    $allUsers = explode(PHP_EOL, shell_exec("cat /etc/passwd | cut -d ':' -f1"));
		array_pop($allUsers);

		foreach($allUsers as $index => $string) {
			if(stripos($string, $keyword) !== FALSE){
				array_push($resultPeople, $string);
			}
		}
		echo '{"searchUsers":'. json_encode($resultPeople) .'}';
	}else{
		echo '{"success":"false","error":"invalidRequest"}';
	}
}else if($_SERVER['REQUEST_METHOD']==="POST"){
	if(isset($_POST['number'])){
		$number = $_POST['number'];
	}else{
		$number = 1;
	}
	if(isset($_POST['user']) && $_POST['user']!==""){
		$user = $_POST['user'];
	}else{
		echo '{"success":"false","error": "noUsernameProvided"}';
		exit;
	}
	if(isset($_POST['message'])){
		$message = $_POST['message'];
	}else{
		$message = "Machines do learn.";
	}
	for($i=0; $i<$number; $i++){
		try{
			shell_exec("echo $message | write $user");
		}catch(Exception $e){
			echo '{"success":"false","error":"cannotSend"}';
		}
	}
	echo '{"success":"true","user":"'.$user.'","number":'.$number.',"message":"'.$message.'"}';	
}	
