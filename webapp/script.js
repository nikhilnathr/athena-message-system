let mainHeading = document.querySelector(".heading h1"),
	mainHeadingText = mainHeading.innerHTML,
	pageHeading = document.querySelector("#page-heading h2"),
	pageHeadingText = pageHeading.innerHTML,
	menu = document.querySelector(".container .nav"),
	menuOverlay = document.querySelector(".nav-overlay"),
	main = document.querySelector(".main"),
  footer = document.querySelector(".footer");

window.addEventListener("scroll", function(e){
	if(window.pageYOffset >=55 && !animated){
		animated = true;
		mainHeading.parentElement.style.boxShadow = "0 0 25px #000";
		mainHeading.style.transition = "all 200ms linear";
		mainHeading.style.transform = "translateY(-50px)";
		setTimeout(function(){
			mainHeading.style.transition = "none";
			mainHeading.style.transform = "translate(0px)";
			mainHeading.innerHTML = pageHeadingText;
			mainHeading.style.transition = "all 200ms linear";
			mainHeading.style.transform = "translateY(0px)";
			pageHeading.innerHTML = ""
		}, 200);
	}else if(window.pageYOffset<40 && animated){
		animated = false;
		mainHeading.style.transition = "all 200ms linear";
		mainHeading.parentElement.style.boxShadow = "none";
		mainHeading.style.transform = "translateY(50px)";
		setTimeout(function(){
			pageHeading.innerHTML = pageHeadingText;
			mainHeading.style.transition = "none";
			mainHeading.style.transform = "translateY(-50px)";
			mainHeading.innerHTML = mainHeadingText;
			mainHeading.style.transition = "all 200ms linear";
			mainHeading.style.transform = "translateY(0px)";
		}, 200);
	}
})

document.querySelector(".menu-btn").addEventListener("click", openMenu);
menuOverlay.addEventListener("click", closeMenu);

function closeMenu(){
	menu.classList.remove("active");
	menuOverlay.style.opacity = 0;
	setTimeout(function(){
		menuOverlay.style.display = "none";
	}, getAnimationTime(menu));
}
function openMenu(){
	menu.classList.add("active");
	menuOverlay.style.display = "block";
	menuOverlay.style.opacity = 1;
}







loadPage("onlineUsers");

document.getElementById("online-users").addEventListener("click", function(){
	loadPage("onlineUsers");
});

document.getElementById("all-users").addEventListener("click", function(){
	loadPage("allUsers");
});

document.getElementById("search").addEventListener("click", function(){
	loadPage("search");
});

function loadPage(page){
	if(isLoading() === "none"){
		startLoading();
	}
	setTimeout(()=>{
		closeMenu();
		setTimeout(()=>{
			if(page==="onlineUsers"){
				let loadOnlineUserPage = new Promise((resolve, reject) => {
					main.innerHTML = "";
					pageHeading.innerHTML = "Online Users";
					const getOnlineUsers="http://athena.nitc.ac.in/nikhil_b170199cs/athena-message-system/api.php?activeUsers";

					fetch(getOnlineUsers)
						.then((data)=>{
							return data.json();
						}).then((response)=>{
							if(response.success){
								pageHeading.innerHTML = "Online Users ("+ response.activeUsers.length  +")";
								if(response.activeUsers.length === 0){
										main.innerHTML += "<div class='no-active-user'><i class='material-icons md-large'>sentiment_dissatisfied</i>Seems like no-one is logged into Athena at this time. So sad ";
								}else{
									for(i=0; user = response.activeUsers[i]; i++){
										main.innerHTML += "<div class='active-user'>"+ user +"</div>";
									}
								}
								resolve();
							}else{
							    showError("Error: "+response.error);
								resolve();
							}
						}).catch((error) => {
                            showError("Error in fetching data");
                            console.log(error);
                            resolve();
						});
				});

				loadOnlineUserPage.then(()=>{
					onLoaded();
				})
			}else if(page==="allUsers"){
				let loadAllUsersPage = new Promise((resolve, reject) => {
					main.innerHTML = "";
					pageHeading.innerHTML = "All Users";
					const getOnlineUsers="http://athena.nitc.ac.in/nikhil_b170199cs/athena-message-system/api.php?allUsers";

					fetch(getOnlineUsers)
						.then((data)=>{
							return data.json();
						}).then((response)=>{
							if(response.success){
								console.log(response);
								Object.entries(response.groups).forEach(
											([key, value]) => {
												groupName = document.createElement("div");
												groupName.classList.add("group");
												groupText = document.createElement("div");
												groupText.classList.add("group-name");
												groupText.appendChild(document.createTextNode(key));
												groupCaret = document.createElement("i");
												groupCaret.classList.add("material-icons");
												groupCaret.appendChild(document.createTextNode("keyboard_arrow_down"));
												groupText.appendChild(groupCaret);
												groupText.addEventListener("click", function(){
													e = this.nextSibling;
													last = this.lastChild;
													if(window.getComputedStyle(last).transform == "matrix(-1, 1.22465e-16, -1.22465e-16, -1, 0, 0)" || last.style.transform == "rotate(180deg)"){
														last.style.transform = "rotate(0deg)";
													}else{
														last.style.transform = "rotate(180deg)";
													}
													this.lastChild.style.transform
													toggleSlide(e);
												});
												groupName.appendChild(groupText);
												groupElements = document.createElement("div");
												groupElements.classList.add("group-members");
												groupElements.classList.add("show-mem");
												for(i=0; i<value.length; i++){
													groupElement = document.createElement("div");
													groupElement.classList.add("group-member");
													groupElement.appendChild(document.createTextNode(value[i]));
													groupElements.appendChild(groupElement);
												}
												groupName.appendChild(groupElements);
												main.appendChild(groupName);
											}
										);
								resolve();
							}else{
                               showError("API error, couldn't fetch data");
								resolve();
							}
						}).catch((error) => {
                                showError("Error in fetching data");
                                resolve();
							});
					});
				loadAllUsersPage.then(()=>{
					onLoaded();
				})
			}else if(page==="search"){
				let loadSearchPage = new Promise((resolve, reject)=>{
					main.innerHTML = "";
					pageHeading.innerHTML = "Search";

					let searchBarContainer = document.createElement("div");
					searchBarContainer.classList.add("main-search-container");

					let searchBar = document.createElement("input");
					searchBar.classList.add("main-search");
					searchBar.setAttribute("placeholder", "Type here...");
					searchBarContainer.appendChild(searchBar);

					let searchResults = document.createElement("div");
					searchResults.classList.add("search-results");

					searchBar.addEventListener("keyup", function(){
						if(ld=document.querySelector(".loading-search")){

						}else{
							let searchLoading = document.createElement("div");
							searchLoading.classList.add("loading-search");
							main.appendChild(searchLoading);

						}
						if(this.value==""){
							searchResults.innerHTML = "";
							main.removeChild(document.querySelector(".loading-search"));
							return;
						}

						//get search results here
						const searchUsers="http://athena.nitc.ac.in/nikhil_b170199cs/athena-message-system/api.php?search="+searchBar.value;

						fetch(searchUsers)
							.then((data)=>{
								return data.json();
							}).then((response)=>{
								if(response.searchUsers.length === 0){
										searchResults.innerHTML = "<div class='no-search-user'><i class='material-icons md-large'>sentiment_dissatisfied</i>No results found.</div> ";
								}else{
									searchResults.innerHTML = "";
									for(i=0; user = response.searchUsers[i]; i++){
										res = document.createElement("div");
										res.classList.add("search-user");
										res.appendChild(document.createTextNode(user));
										searchResults.appendChild(res);
									}
								}
								if(ld=document.querySelector(".loading-search")){
									ld.parentNode.removeChild(ld);
								}
							});
					});
					main.appendChild(searchBarContainer);
					main.appendChild(searchResults);
					resolve();

				});
				loadSearchPage.then(()=>{
					onLoaded();
				}).catch((error)=>{
                    showError("Syntax error in code");
					console.log(error);
				});
			}
		}, 500);
	}, 420);

}

function showError(msg = "Error !"){
    let err = document.createElement("div");
    err.classList.add("error");
    let errMsg = document.createElement("div");
    errMsg.append(document.createTextNode(msg));
    err.append(errMsg);
    let closeBtn = document.createElement("div");
    closeBtn.classList.add("closeBtn");
    closeBtn.setAttribute("onclick", "closeDialog(this.parentNode)");
    let closeIcon = document.createElement("i");
    closeIcon.classList.add("material-icons");
    closeIcon.append(document.createTextNode("close"));

    closeBtn.append(closeIcon);
    err.append(closeBtn);
    main.append(err);
}
getHeight = function(el) {
		var el_style = window.getComputedStyle(el),
			el_display    = el_style.display,
			el_position   = el_style.position,
			el_visibility = el_style.visibility,
			el_max_height = el_style.maxHeight.replace('px', '').replace('%', ''),

			wanted_height = 0;

			if(el_display !== 'none' && el_max_height !== '0') {
				return el.offsetHeight;
			}


			el.style.position   = 'absolute';
			el.style.visibility = 'hidden';
			el.style.display    = 'block';

			wanted_height     = el.offsetHeight;


			el.style.display    = el_display;
			el.style.position   = el_position;
			el.style.visibility = el_visibility;

			return wanted_height;
};


toggleSlide = function(el) {
	var el_max_height = 0;

	if(el.getAttribute('data-max-height')) {

		if(el.style.maxHeight.replace('px', '').replace('%', '') === '0') {
			el.style.maxHeight = el.getAttribute('data-max-height');
		}else{
			el.style.maxHeight = '0';
		}
	}else{
		el_max_height                  = getHeight(el) + 'px';
		el.style['transition']         = 'max-height 1s ease-in-out';
		el.style.overflowY             = 'hidden';
		el.style.maxHeight             = '0';
		el.setAttribute('data-max-height', el_max_height);
		el.style.display               = 'block';


		setTimeout(function() {
			el.style.maxHeight = el_max_height;
		}, 10);
	}
};

addRipple(document.querySelector(".quick-nav"));

function addRipple(el, bgColor = "rgba(255, 255, 255, 0.4)", tme = 600) {
    el.addEventListener("click", function (e) {

        el.style.overflow = "hidden";

        //CODE TO REMOVE MULTIPLE RIPPLES BUT THEY SEEM COOLER SO COMMENTED IT TEMPORARILY
        //for (let i = 0; rip = document.querySelectorAll(".ripple")[i]; i++) {
         //   rip.parentNode.removeChild(rip);
        //}

        let posX = this.offsetLeft,
            posY = this.offsetTop,
            rect = this.getBoundingClientRect(),
            buttonWidth = rect.width,
            buttonHeight = rect.height;

        let ripple = document.createElement("span");
        ripple.classList.add('ripple');
        ripple.style.background = bgColor;
        ripple.style.width = "0px";
        ripple.style.height = "0px";
        ripple.style.borderRadius = "50%";
        ripple.style.transform = "scale(0)";
        ripple.style.position = "absolute";
        ripple.style.opacity = "1";

        this.prepend(ripple);


        if (buttonWidth >= buttonHeight) {
            buttonHeight = buttonWidth;
        } else {
            buttonWidth = buttonHeight;
        }

        let x = e.pageX - posX - buttonWidth / 2,
            y = e.pageY - posY - buttonHeight / 2;


        ripple.style.height = buttonHeight + "px";
        ripple.style.width = buttonWidth + "px";
        ripple.style.top = y + 'px';
        ripple.style.left = x + 'px';
        ripple.style.animation = "rippleDrop "+ tme +"ms linear";
    });
}
