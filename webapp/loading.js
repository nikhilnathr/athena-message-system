let animated = false;
function onLoaded() {
    let loadingScreen = document.querySelector(".loading-screen");
    loadingScreen.style.opacity = 0;
    deleteAfter(loadingScreen, getAnimationTime(loadingScreen));
}
function startLoading(){
    let loadingScreen = document.querySelector(".loading-screen");
	loadingScreen.style.display = "flex";
	setTimeout(()=>{
		loadingScreen.style.opacity = 1;
	}, 20);
}

function deleteAfter(e, time){
    setTimeout(function(){
        e.style.display = "none";
    }, time);
}
function getAnimationTime(e){
    return parseFloat(getComputedStyle(e)['transitionDuration']) * 1000;
}

function closeDialog(e){
	e.style.transition = "transform 400ms linear";
	e.style.transform = "translateY(100%)";
	setTimeout(()=>{
		e.parentNode.removeChild(e);
	}, getAnimationTime(e))
}
function isLoading(){
	element = document.querySelector(".loading-screen");
	return (element.currentStyle ? element.currentStyle.display : getComputedStyle(element, null).display);
}
